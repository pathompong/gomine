package raknet

import (
	"net"
)

type Peer struct {
	socket *UDPSocket
}

func NewPeer() *Peer {
	return &Peer{}
}

func (p *Peer) Startup(socket *UDPSocket) {
	p.socket = socket
}

func (p *Peer) Connect(remote string) error {
	a, err := net.ResolveUDPAddr("udp", remote)
	if err != nil {
		return err
	}

	p.socket.conn, err = net.DialUDP("udp", nil, a)
	if err != nil {
		return err
	}

	return nil
}

func (p *Peer) SetMaximumIncomingConnections(max int) {
}

func (p *Peer) Receive() ([]byte, *net.UDPAddr, error) {
	data := make([]byte, 1500)
	_, r, err := p.socket.conn.ReadFromUDP(data)
	if err != nil {
		return nil, nil, err
	}

	return data, r, nil
}

func (p *Peer) Send(data []byte, remote *net.UDPAddr) error {
	_, err := p.socket.conn.WriteToUDP(data, remote)
	return err
}

func (p *Peer) Shutdown() {
	p.socket.conn.Close()
}
