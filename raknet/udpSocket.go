package raknet

import "net"

type UDPSocket struct {
	conn *net.UDPConn
}

func NewUDPSocket() (*UDPSocket, error) {
	return &UDPSocket{}, nil
}

func NewUDPServerSocket(addr string) (*UDPSocket, error) {
	a, err := net.ResolveUDPAddr("udp", addr)
	if err != nil {
		return nil, err
	}

	s := &UDPSocket{}
	s.conn, err = net.ListenUDP("udp", a)
	if err != nil {
		return nil, err
	}

	return s, nil
}
