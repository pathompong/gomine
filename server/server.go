package server

import (
	"log"
	"math/rand"
	"net"

	"bitbucket.org/pathompong/gomine/handlers"
	"bitbucket.org/pathompong/gomine/raknet"
	"bitbucket.org/pathompong/gomine/session"
)

type Server struct {
	serverId int64
	peer     *raknet.Peer
	exited   chan bool
	sessions map[string]*session.Session
}

func New() *Server {
	return &Server{
		serverId: rand.Int63(),
		peer:     raknet.NewPeer(),
		exited:   make(chan bool),
		sessions: make(map[string]*session.Session),
	}
}

func (s *Server) ServerId() int64 {
	return s.serverId
}

func (s *Server) processPacket(remote *net.UDPAddr, data []byte) {
	sess, ok := s.sessions[remote.String()]
	if !ok {
		log.Printf("Creating a new session for %s\n", remote.String())
		sess = &session.Session{
			Server: s,
			Peer:   s.peer,
			Remote: remote,
		}
		s.sessions[remote.String()] = sess
	}

	err := handlers.Handle(sess, data)
	if err != nil {
		log.Printf("Error processing packet for %s: %s\n",
			remote.String(), err.Error())
	}
}

func (s *Server) Serve() error {
	log.Print("GoMine server is serving")

	socket, err := raknet.NewUDPServerSocket(":19132")
	if err != nil {
		log.Printf("Error creating socket: %s\n", err.Error())
	}
	s.peer.Startup(socket)

	go func() {
		for {
			data, remoteAddr, err := s.peer.Receive()
			if err != nil {
				break
			}
			s.processPacket(remoteAddr, data)
		}

		s.exited <- true
	}()

	return nil
}

func (s *Server) Stop() {
	log.Printf("GoMine server is exiting")

	s.peer.Shutdown()
	<-s.exited
}
